const express = require("express");
const cors = require("cors");
const userRouter = require("./routes/userRoute");
const courseRouter = require("./routes/courseRoute");
const dbConnection = require("./dbConnection");

const app = express();
dbConnection();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/users", userRouter);
app.use("/courses", courseRouter);

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Listening on port ${port}...`));
