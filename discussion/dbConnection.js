const mongoose = require("mongoose");

module.exports = () => {
    const connectionParams = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    };
    try {
        mongoose.connect(
            "mongodb+srv://admin123:admin123@wdc028-course-booking.rtpbcvq.mongodb.net/b241API?retryWrites=true&w=majority",
            connectionParams,
            mongoose.set("strictQuery", false)
        );
        console.log("Connected to Atlas");
    } catch (error) {
        console.log("Connection Error");
    }
};
