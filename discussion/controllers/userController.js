const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

function checkEmailExist(req, res) {
    const requestBody = req.body;
    return User.find({ email: requestBody.email })
        .then((result) => {
            if (result.length > 0) {
                return true;
            } else {
                return false;
            }
        })
        .then((resultFromController) => res.send(resultFromController));
}

function registerUser(req, res) {
    const requestBody = req.body;
    return User.findOne({ email: requestBody.email })
        .then((result) => {
            if (result != null && result.email == requestBody.email) {
                res.status(401).send({ message: "Email already exists" });
            } else {
                let newUser = new User({
                    firstName: requestBody.firstName,
                    lastName: requestBody.lastName,
                    email: requestBody.email,
                    password: bcrypt.hashSync(requestBody.password, 10),
                    mobileNo: requestBody.mobileNo,
                });
                return newUser.save().then((saveResult, saveErr) => {
                    if (saveErr) {
                        res.status(400).send({
                            message: "Error in adding user",
                        });
                    } else {
                        res.status(201).send({
                            message: "User added",
                        });
                    }
                });
            }
        })
        .then((resultFromController) => res.send(resultFromController));
}
function loginUser(req, res) {
    const requestBody = req.body;
    return User.findOne({ email: requestBody.email })
        .then((result) => {
            if (result == null) {
                return false;
            } else {
                const isPasswordCorrect = bcrypt.compareSync(
                    requestBody.password,
                    result.password
                );
                if (isPasswordCorrect) {
                    return { access: auth.createAccessToken(result) };
                } else {
                    return false;
                }
            }
        })
        .then((resultFromController) => res.send(resultFromController));
}

function detailsUser(req, res) {
    const userData = auth.decode(req.headers.authorization);
    return User.findById({ _id: userData.id }).then((result) => {
        result.password = "";
        res.status(200).send(result);
    });
}
async function enrollUser(req, res) {
    try {
        const userId = auth.decode(req.headers.authorization).id;
        const courseId = req.params.courseId;
        const user = await User.findById({ _id: userId });
        const course = await Course.findById({ _id: courseId });
        if (!user) return res.status(400).json({ message: "User not found" });
        if (user.isAdmin)
            return res
                .status(400)
                .json({ message: "Admin not permitted to enroll courses" });
        if (!course)
            return res.status(400).json({ message: "Course is not available" });
        user.enrollments.push({ courseId: course._id });
        await user.save();

        course.enrollees.push({ userID: user._id });
        await course.save();
        res.status(201).json({ message: "Course added successfully" });
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

// module.exports = async function checkEmailExist(req, res) {
//     try {
//         const requestBody = req.body;
//         const result = await User.find({ email: requestBody.email });
//         const resultFromController = result.length > 0;
//         res.send(resultFromController);
//     } catch (error) {
//         console.error(error);
//         res.status(500).send({ message: "Error in checking emails" });
//     }
// };

// async function registerUser(req, res) {
//     try {
//         const requestBody = req.body;
//         const user = await User.findOne({ firstName: requestBody.firstName });

//         if (user != null && user.firstName === requestBody.firstName) {
//             return res.status(401).send({ message: "User already exists" });
//         }

//         const newUser = new User({
//             firstName: requestBody.firstName,
//             lastName: requestBody.lastName,
//             email: requestBody.email,
//             password: requestBody.password,
//             mobileNo: requestBody.mobileNo,
//         });
//         await newUser.save();

//         return res.status(201).send({ message: "User added" });
//     } catch (error) {
//         return res.status(400).send({ message: "Error in adding user" });
//     }
// }

module.exports = {
    checkEmailExist,
    registerUser,
    loginUser,
    detailsUser,
    enrollUser,
};
