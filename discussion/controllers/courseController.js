const Course = require("../models/Course");
const User = require("../models/User");
const auth = require("../auth");

function creatingCourse(req, res) {
    const userData = auth.decode(req.headers.authorization);
    return User.findOne({ isAdmin: userData.isAdmin }).then((result) => {
        if (result.isAdmin === userData.isAdmin) {
            let newCourse = new Course({
                name: req.body.name,
                description: req.body.description,
                price: req.body.price,
            });
            return newCourse.save().then((course, error) => {
                if (error) {
                    return false;
                } else {
                    return res.status(201).send({ message: "true hehe" });
                }
            });
        } else {
            return false;
        }
    });
}

async function displayCourse(req, res) {
    try {
        const course = await Course.find({
            name: { $regex: req.body.name, $options: "i" },
        });
        if (!course.length)
            return res.status(400).json({ message: "No course available" });
        res.status(200).json({ course });
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

async function updateCourse(req, res) {
    try {
        const courseId = req.params.courseId;
        const userData = auth.decode(req.headers.authorization);
        const course = await Course.findById({ _id: courseId });
        if (!course)
            return res.status(400).json({ message: "No such course found." });
        if (!userData.isAdmin)
            return res.status(400).json({ message: "User is not admin" });

        course.name = req.body.name;
        course.description = req.body.description;
        course.price = req.body.price;
        await course.save();

        res.status(200).json({ message: "Course updated" });
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}
async function archiveCourse(req, res) {
    try {
        const courseId = req.params.courseId;
        const userData = auth.decode(req.headers.authorization).isAdmin;
        const course = await Course.findById({ _id: courseId });
        if (!course)
            return res.status(400).json({ message: "No such course found" });
        if (!userData)
            return res.status(400).json({ message: "User is not admin" });

        course.isActive = false;
        await course.save();
        res.status(200).json({ message: "Course archived" });
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

module.exports = {
    creatingCourse,
    displayCourse,
    updateCourse,
    archiveCourse,
};
