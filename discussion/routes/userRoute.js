const express = require("express");
const router = express.Router();
const {
    checkEmailExist,
    registerUser,
    loginUser,
    detailsUser,
    enrollUser,
} = require("../controllers/userController");
const auth = require("../auth");

router.post("/checkEmail", checkEmailExist);
router.post("/register", registerUser);
router.post("/login", loginUser);
router.post("/details", auth.verify, detailsUser);
router.post("/:courseId/enroll", auth.verify, enrollUser);

module.exports = router;
