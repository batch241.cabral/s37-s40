const express = require("express");
const router = express.Router();
const {
    creatingCourse,
    displayCourse,
    updateCourse,
    archiveCourse,
} = require("../controllers/courseController");
const auth = require("../auth");

router.post("/", auth.verify, creatingCourse);
router.get("/all", displayCourse);
router.put("/update/:courseId", auth.verify, updateCourse);
router.patch("/:courseId/archive", auth.verify, archiveCourse);

module.exports = router;
